package com.group2.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.group2.api.model.Student;
import com.group2.api.repository.StudentRepository;

/**
 * The StudentServiceImpl Implementation Class
 * <p>
 * This class overrides the imposed methods that are in the StudentService
 * Interface class
 * </p>
 * <p>
 * by implementing the said class interface class.
 * </p>
 * 
 * @see com.group2.api.services.StudentService
 * @see com.group2.api.repository.StudentRepository
 * @author Group 2
 */
@Service
public class StudentServiceImpl implements StudentService {
    // the student service implementation class is where we impose / implements the
    // functions that we have defined earlier in the student service class
    private static StudentServiceImpl studentService;

    private StudentRepository studentRepository;

    private StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    /**
     * Create Student Method
     * <p>
     * This method uses the student repository's save method
     * </p>
     * <p>
     * to save new student information, in this case
     * </p>
     * 
     * @param student it takes a student parameter to then be
     * @return as a new student.
     */
    @Override
    public Student createStudent(Student student) {
        return studentRepository.save(student);
    }

    /**
     * Paginate All Student Method
     * <p>
     * This method paginates the student list
     * </p>
     * <p>
     * This method uses the pre-defined findAll method from the student repository
     * class.
     * </p>
     * 
     * @param offSet to identify the number of the student information before
     * @return it returns a certain amount of student in the list.
     */
    @Override
    public Page<Student> paginateAllStudent(int offSet) {
        Page<Student> studentPage = studentRepository.findAll(PageRequest.of(offSet, 5));
        return studentPage;
    }

    /**
     * Get All Student Method.
     * <p>
     * This method is to get all the students that are in the database.
     * </p>
     * <p>
     * This method uses the pre-defines findAll method from the student repository.
     * </p>
     * 
     * @return student list
     */
    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }

    /**
     * Get Student By Id Method
     * <p>
     * This method gets a student from the database
     * </p>
     * <p>
     * by specifying the student Id.
     * </p>
     * <p>
     * This method also uses a pre-defined method from the student repository
     * </p>
     * <p>
     * which is the findById method.
     * </p>
     * 
     * @param studentId is used to specify which student to return.
     * @return student
     */
    @Override
    public Student getStudentById(Long studentId) {
        return studentRepository.findById(studentId).orElse(null);
    }

    /**
     * Update Student Method
     * <p>
     * This method updates an existing student record by specifying the student Id.
     * </p>
     * <p>
     * This method also uses the save method from the student repository,
     * </p>
     * <p>
     * the only difference is that all the existing information are to get first.
     * </p>
     * 
     * @param studentId to specify which student information to update
     * @param student   to get the existing student information
     * @return update student
     */
    @Override
    public Student updateStudent(Long studentId, Student student) {
        Student existingStudent = studentRepository.findById(studentId).orElse(null);
        existingStudent.setStudentName(student.getStudentName());
        existingStudent.setAge(student.getAge());
        existingStudent.setAddress(student.getAddress());
        existingStudent.setZip(student.getZip());
        existingStudent.setCity(student.getCity());
        existingStudent.setProvinceAndRegion(student.getProvinceAndRegion());
        existingStudent.setPhoneNumber(student.getPhoneNumber());
        existingStudent.setMobileNumber(student.getMobileNumber());
        existingStudent.setEmail(student.getEmail());
        existingStudent.setYearLevel(student.getYearLevel());
        existingStudent.setSection(student.getSection());

        studentRepository.save(existingStudent);
        return existingStudent;
    }

    /**
     * The Delete Student Method
     * <p>
     * This method is used for the deletion of a student and all of their records in
     * the database.
     * </p>
     * <p>
     * This method use the pre-defined deleteById method from the student
     * repository.
     * </p>
     * <p>
     * This method checks first if the student is existing, if yes then the student
     * will be deleted.
     * </p>
     * 
     * @param studentId used to first check the existence of teh record and then
     *                  delete the student
     *                  record if it exists.
     * @return boolean deleted as a response.
     */
    @Override
    public boolean deleteStudent(Long studentId) {
        boolean deleted = false;
        Optional<Student> student = studentRepository.findById(studentId);
        if (student.isPresent()) {
            studentRepository.deleteById(studentId);
            deleted = true;
        }
        return deleted;
    }

    /**
     * Get By Year Level and Section Method
     * <p>
     * This method is used to filter the list of students in the database.
     * </p>
     * <p>
     * This method groups the data by year-level, and section.
     * </p>
     * <p>
     * This method uses a special query from the student repository class.
     * </p>
     * 
     * @return grouped list of student by year level and section
     */
    @Override
    public List<Object[]> getByYearLevelSection() {
        return studentRepository.getByYearLevelSection();
    }

    /**
     * Get By Year Level, Section, City and ZipMethod
     * <p>
     * This method filters and sorts the list of student in the database by their
     * </p>
     * <p>
     * year level, section, city and zip.
     * </p>
     * <p>
     * This method also uses a special query that can be found in the student
     * repository class
     * </p>
     * 
     * @return filtered and sorted list of student by year level, section, city and
     *         zip.
     */
    @Override
    public List<Object[]> getByYearLevelSectionCityZip() {
        return studentRepository.getByYearLevelSectionCityZip();
    }

    /**
     * Get By Age Method
     * <p>
     * This method is used to get all the students in between a certain age bracket.
     * </p>
     * <p>
     * This method also uses a special query that can be found in the student
     * repository class.
     * </p>
     * 
     * @param minAge this parameter specifies the <b>minimum</b> age that the system
     *               needs to get.
     * @param maxAge this parameter specifies the <b>maximum </b> age that the
     *               system needs to get.
     * @return list of student in an age bracket inputted by the user.
     */
    @Override
    public List<Object[]> getByAge(int minAge, int maxAge) {
        return studentRepository.getByAge(minAge, maxAge);
    }
}