package com.group2.api;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The Student Information System Api Application Class
 * <p>
 * This class enables the use of Swagger
 * </p>
 * 
 * @author Group 2
 */
@SpringBootApplication
@EnableSwagger2
public class StudentInformationSystemApiApplication {

    /**
     * This is the main method of this whole system.
     * <p>
     * Running this method would start the whole spring boot project.
     * </p>
     */
    public static void main(String[] args) {
        SpringApplication.run(StudentInformationSystemApiApplication.class, args);
    }

    /**
     * <p>
     * This method configures the Docket Bean
     * </p>
     * <p>
     * To successfully integrate Swagger into the system.
     * </p>
     * <p>
     * Automatically making a documentation for our com.group2.api.controller
     * </p>
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.group2.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiDetails());
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Student Information System API",
                "API for Student Information System",
                "1.0",
                "Free to use",
                new springfox.documentation.service.Contact("Group2", "", ""),
                "API License",
                "",
                Collections.emptyList());
    }
}